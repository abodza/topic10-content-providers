package cs518.sample.usecalendarcontentprovider

/*
 * Uses the Calendar Content Provider
 * needs the following in the manifest
 *  <uses-permission android:name="android.permission.READ_CALENDAR" />
 *  If you add writing functionality you will also need:
 *  <uses-permission android:name="android.permission.WRITE_CALENDAR" />
 *
 *  following
 *  http://developer.android.com/guide/topics/providers/content-providers.html
 *  http://developer.android.com/guide/topics/providers/calendar-provider.html
 *  This is a simple implementation I leave it to you to implement
 *  a CursorAdapter and an AdapterView and and other functionality.
 */
import android.app.Activity
import android.database.Cursor
import android.os.Bundle
import android.provider.CalendarContract.Calendars
import android.widget.TextView

class MainActivity : Activity() {
    private // Run query, read all calendars on device

    val calendar: Cursor?
        get() {
            val uri = Calendars.CONTENT_URI
            val projection = arrayOf(Calendars._ID, Calendars.NAME, Calendars.ACCOUNT_NAME, Calendars.ACCOUNT_TYPE)

            val selection = (Calendars.VISIBLE + " = '"
                    + "1" + "'")
            val selectionArgs: Array<String>? = null

            val sortOrder = Calendars._ID + " COLLATE LOCALIZED ASC"
            return contentResolver.query(uri, projection, selection, selectionArgs,
                    sortOrder)
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val tView = findViewById(R.id.calendarview) as TextView
        val cursor = calendar

        while (cursor!!.moveToNext()) {
            val displayName = cursor.getString(cursor
                    .getColumnIndex(Calendars.NAME))
            tView.append("Name: ")
            tView.append(displayName)
            tView.append("\n")
        }
        cursor.close()
    }

}
