package cs518.sample.usemediastorecontentprovider

/*
 * Uses the MediaStore Content Provider
 * needs the following in the manifest
 *  <uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE" />
 *  <uses-permission android:name="android.permission.READ_INTERNAL_STORAGE" />
 *  follow your book
 *  This is a simple implementation I leave it to you to implement
 *  a CursorAdapter and an AdapterView and and other functionality.
 */
import android.app.Activity
import android.database.Cursor
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.widget.TextView

class MainActivity : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        var cursor: Cursor?
        var counts: String
        var count = 50
        val tView = findViewById(R.id.calendarview) as TextView

        cursor = getAudio(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI)
        counts = "External audio files: " + cursor!!.count + " (Showing 50)\n"
        tView.append(counts)

        while (cursor.moveToNext() && count-- > 0) {
            val displayName = cursor.getString(cursor
                    .getColumnIndex(MediaStore.Audio.Media.TITLE))
            tView.append("Name: ")
            tView.append(displayName)
            val lengSeconds = (cursor.getInt(cursor
                    .getColumnIndex(MediaStore.Audio.Media.DURATION)) / 1000).toString() + "s"
            tView.append(" Seconds: ")
            tView.append(lengSeconds)
            tView.append("\n")
        }
        cursor.close()

        cursor = getAudio(MediaStore.Audio.Media.INTERNAL_CONTENT_URI)
        counts = "Internal audio files: " + cursor!!.count + " (Showing 50)\n"
        tView.append(counts)
        count = 50
        while (cursor.moveToNext() && count-- > 0) {
            val displayName = cursor.getString(cursor
                    .getColumnIndex(MediaStore.Audio.Media.TITLE))
            tView.append("Name: ")
            tView.append(displayName)
            val lengSeconds = (cursor.getInt(cursor
                    .getColumnIndex(MediaStore.Audio.Media.DURATION)) / 1000).toString() + "s"
            tView.append(" Seconds: ")
            tView.append(lengSeconds)
            tView.append("\n")
        }
        cursor.close()
    }

    private fun getAudio(uri: Uri): Cursor? {
        // Run query, read all audio files on
        val projection = arrayOf(MediaStore.Audio.Media.TITLE, MediaStore.Audio.Media.DURATION)
        val selection: String? = null
        val selectionArgs: Array<String>? = null
        val sortOrder: String? = null
        /**
         * Not using SQLiteDatabawe.query(table, ...)
         * using ContentResolver.query(uri, ...)
         */
        return contentResolver.query(uri, projection, selection, selectionArgs,
                sortOrder)
    }

}
